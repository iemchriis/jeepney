﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class MainUIManager : MonoBehaviour
{

    public GameObject MainCanvas;
    public GameObject CircuitCanvas;
    public GameObject EndlessCanvas;
    public GameObject SprintCanvas;

    private GameObject SelectedCanvas;
    private string selectedSceneName;
    private EventSystem eventSystem;


    private void Start()
    {
        eventSystem = GameObject.FindObjectOfType<EventSystem>();
    }

    private void Update()
    {
        if(Input.GetButton("Cancel"))
        {
            BackScreen();
        }
    }

    public void ChangeScreen(string scene)
    {


        switch(scene)
        {

            case "Circuit":
                SelectedCanvas = CircuitCanvas;
             
            break;

            case "Endless":
                SelectedCanvas = EndlessCanvas;
            break;

            case "Sprint":
                SelectedCanvas = SprintCanvas;
                break;

        }
        selectedSceneName = scene;
        MainCanvas.SetActive(false);
        eventSystem.SetSelectedGameObject(SelectedCanvas.transform.GetChild(0).transform.GetChild(0).gameObject);
        SelectedCanvas.SetActive(true);
    }

    void BackScreen()
    {
        if (SelectedCanvas != null)
        {
            MainCanvas.SetActive(true);
            eventSystem.SetSelectedGameObject(MainCanvas.transform.GetChild(0).gameObject);
            SelectedCanvas.SetActive(false);
            SelectedCanvas = null;
        }
    }


    public void ChoosePlayerCount(int num)
    {

        PlayerDataManager.Instance.NumberOfPlayers = num;
        SceneManager.LoadScene(selectedSceneName);
    }

}
