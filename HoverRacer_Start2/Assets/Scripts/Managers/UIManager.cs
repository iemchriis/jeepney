﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; set; }
    public Slider HealthSlider;
    public TextMeshProUGUI Score;

    private void Awake()
    {
        Instance = this;
    }
}

