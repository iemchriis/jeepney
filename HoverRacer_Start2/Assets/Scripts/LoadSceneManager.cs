﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public static class LoadSceneManager
{
    public static void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

}
