﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler current;
    public int pooledAmount = 5;
    public GameObject pooledObject;
    public GameObject container;

    public List <GameObject> pooledObjects = new List<GameObject>();


    private void Awake()
    {
        current = this;
    }
    

    private void Start()
    {
        pooledObjects = new List<GameObject>();
        for(int i = 0; i < 5; i++)
        {
            GameObject objSpawn = (GameObject)Instantiate(pooledObject);
            objSpawn.SetActive(false);
            pooledObjects.Add(objSpawn);

        }
    }

    public GameObject GetPooledObject()
    {

        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if(!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }
        return null;
    }




}
