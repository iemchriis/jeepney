﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System;
using Random = UnityEngine.Random;

public class ObstaclesWayPoints : MonoBehaviour
{
    public static ObstaclesWayPoints Instance { get; set; }
    public List<Transform> waypoints = new List<Transform>();
    public Transform targetWaypoint;
    private int targetWaypointIndex = 0;
    private float minDistance = 0.1f;
    private int lastWaypointIndex;

    private float movementSpeed = 50.0f;
    private float rotationSpeed = 2.0f;
    
    [SerializeField]
    public GameObject SelectedWaypoint;

    public GameObject SelectRoute;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        AssigningWaypoints();
        if (waypoints != null)
        {       
            lastWaypointIndex = waypoints.Count - 1;
            targetWaypoint = waypoints[targetWaypointIndex];
        }
        
    }

    private void Update()
    {
        if (waypoints != null && targetWaypoint != null)
        { 
            float movementStep = movementSpeed * Time.deltaTime;
            //float rotationStep = rotationSpeed * Time.deltaTime;

            Vector3 directionToTarget = targetWaypoint.position - transform.position;
            //Quaternion rotationToTarget = Quaternion.LookRotation(directionToTarget);
        
            //transform.rotation = Quaternion.Slerp(transform.rotation, rotationToTarget, rotationStep);
            float distance = Vector3.Distance(transform.position, targetWaypoint.position);
            CheckDistanceToWaypoint(distance);

            transform.position = Vector3.MoveTowards(transform.position, targetWaypoint.position, movementStep);
        }
    }
    void CheckDistanceToWaypoint(float currentDistance)
    {
        if (currentDistance <= minDistance)
        {
            targetWaypointIndex++;
            UpdateTargetWaypoint();
        }
    }

    void UpdateTargetWaypoint()
    {
        if (targetWaypoint != null)
        {
            if (targetWaypointIndex > lastWaypointIndex)
            {
                targetWaypointIndex = 0;
            }

            targetWaypoint = waypoints[targetWaypointIndex];
        }

    }

    void AssigningWaypoints()
    {
        if (SelectedWaypoint != null)
        {
             SelectRoute = SelectedWaypoint.transform.GetChild(Random.Range(0,SelectedWaypoint.transform.childCount)).gameObject;
            if(SelectRoute != null)
           // Debug.Log(SelectedWaypoint.gameObject.transform.GetChild(0).name);
            for (int i = 0; i < SelectRoute.transform.childCount; i++)
            {
                waypoints.Add(SelectRoute.transform.GetChild(i));
            }
        }
    }
}
