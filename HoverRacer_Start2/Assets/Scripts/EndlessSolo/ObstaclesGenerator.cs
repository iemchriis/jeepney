﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesGenerator : MonoBehaviour
{

    public static ObstaclesGenerator Instance { get; set;}
    public List<GameObject> ObstaclesSpawn = new List<GameObject>();
    public GameObject[] ObstacleSpawnPoints;
    public GameObject SpawnObject;
    float radius = .5f;
    public Transform CurrentTrack;
    public bool checker = true;

    public int needObstacles = 5;

    GameObject randomSpawn;
    Vector3 CheckPos;
    
    private void Awake()
    {
        Instance = this;
    }
    public Transform PickObstaclesToSpawn()
    {
        Transform PickedObstacles = null;
        int Picknum = Random.Range(0, ObstaclesSpawn.Count);
        PickedObstacles = ObstaclesSpawn[Picknum].transform;
        return PickedObstacles;
    }

    private void Start()
    {
        for (int i = 0; i < needObstacles; i++)
        {
            PuttingObstaclesOnTrack();
        }
    }

    public void PuttingObstaclesOnTrack()
    {
        SpawnObject = ObstaclesSpawn[Random.Range(0, ObstaclesSpawn.Count)];
        if (SpawnObject != null)
        {
            GameObject currentSpawnObject = (GameObject) Instantiate(SpawnObject);
            currentSpawnObject.gameObject.tag = "Obstacle";
            currentSpawnObject.transform.SetParent(CurrentTrack);
            currentSpawnObject.gameObject.transform.localPosition = CheckingSpawnPoint();
        }
    }

    public Vector3 CheckingSpawnPoint()
    {

        while (checker)
        { 
            randomSpawn = ObstacleSpawnPoints[Random.Range(0, ObstacleSpawnPoints.Length)];
            if(!Physics.CheckSphere(randomSpawn.transform.position,radius))
            {
                ObstaclesWayPoints.Instance.SelectedWaypoint = randomSpawn.gameObject;
                CheckPos = randomSpawn.transform.localPosition;
                return CheckPos;
            }
        }
        return Vector3.zero;
    }
    
}
