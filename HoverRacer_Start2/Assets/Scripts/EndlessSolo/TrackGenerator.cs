﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TrackGenerator : MonoBehaviour
{
    public TrackCreate[] trackCreate;
    public TrackCreate FirstTrack;
    

    private TrackCreate PreviousTrack;

    public Vector3 SpawnOrigin;

    private Vector3 SpawnPosition;
    public int TrackToSpawn = 10;

    void OnEnable()
    {
        TriggerExit.OnChunkExited += PickandSpawnTrack;
    }
    private void OnDisable()
    {
        TriggerExit.OnChunkExited -= PickandSpawnTrack;
    }


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {

            PickandSpawnTrack();
        }
    }


    private void Start()
    {
        PreviousTrack = FirstTrack;

        for(int i = 0; i< TrackToSpawn; i ++)
        {
            PickandSpawnTrack();
        }

    }

    TrackCreate PickNextTrack()
    {
        List<TrackCreate> allowedTrackList = new List<TrackCreate>();
        TrackCreate nextTrack = null;

        TrackCreate.Direction nextRequiredDirection = TrackCreate.Direction.North;

        switch(PreviousTrack.ExitDirection)
        {
            case TrackCreate.Direction.North:
                nextRequiredDirection = TrackCreate.Direction.South;
                SpawnPosition = SpawnPosition + new Vector3(0f, 0, PreviousTrack.TrackSize.y);
                break;

            case TrackCreate.Direction.East:
                nextRequiredDirection = TrackCreate.Direction.West;
                SpawnPosition = SpawnPosition + new Vector3(PreviousTrack.TrackSize.x, 0, 0);
                break;
            case TrackCreate.Direction.South:
                nextRequiredDirection = TrackCreate.Direction.North;
                SpawnPosition = SpawnPosition + new Vector3(0f, 0, -PreviousTrack.TrackSize.y);
                break;
            case TrackCreate.Direction.West:
                nextRequiredDirection = TrackCreate.Direction.East;
                SpawnPosition = SpawnPosition + new Vector3(-PreviousTrack.TrackSize.y, 0, 0);
                break;
            default:
                break;

        }

        for (int i = 0; i < trackCreate.Length; i++)
        {
            if(trackCreate[i].EntryDirection == nextRequiredDirection)
            {
                allowedTrackList.Add(trackCreate[i]);

            }
        }

        nextTrack = allowedTrackList[Random.Range(0, allowedTrackList.Count)];

        return nextTrack;


    }

    void PickandSpawnTrack()
    {
        TrackCreate tracktoSpawn = PickNextTrack();
        GameObject objectFromTrack = tracktoSpawn.Tracks[Random.Range(0, tracktoSpawn.Tracks.Length)];
        PreviousTrack = tracktoSpawn;
        Instantiate(objectFromTrack, SpawnPosition + SpawnOrigin, Quaternion.identity);
    }


    public void UpdateSpawnOrigin(Vector3 originDelta)
    {
        SpawnOrigin = SpawnOrigin + originDelta;
    }

}

