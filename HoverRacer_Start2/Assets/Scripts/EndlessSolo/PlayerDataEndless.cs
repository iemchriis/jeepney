﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class PlayerDataEndless : MonoBehaviour
{
    public static PlayerDataEndless Instance { get; set; }
    public int staringHealth = 100;
    public int currentHealth;
    public int Score;

    private Rigidbody rigidbody;
    private double ForScoring;

    private void Awake()
    {
        Instance = this;
    }

    public void FixedUpdate()
    {
        ForScoring = Math.Round((rigidbody.velocity.magnitude));
        if (ForScoring > 0)
        {
            Score++;
            UIManager.Instance.Score.text = Score.ToString();
        }
   
    }

    public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        currentHealth = staringHealth;
    }

    public void TakenDamage(int amount)
    {
        currentHealth -= amount;
        UIManager.Instance.HealthSlider.value = currentHealth;
    }

    public void SettingScore()
    {
        Score++;
        UIManager.Instance.Score.text = Score.ToString();
    }

public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            TakenDamage(10);
        }
    }
}