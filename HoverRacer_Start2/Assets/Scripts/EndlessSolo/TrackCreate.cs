﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName= "TrackCreate")]
public class TrackCreate : ScriptableObject
{
    public enum Direction
    {
        North, East, South, West
    }

    public Vector2 TrackSize = new Vector2(10, 20f);

    public GameObject[] Tracks;
    public Direction EntryDirection;
    public Direction ExitDirection;

}
