﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{

    public static PlayerDataManager Instance { get; set; }

    public int NumberOfPlayers;



    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if(Instance == null)
        {
  
            Instance = this;

        }
        else
        {
            Destroy(gameObject);
        }

       
    }



}
