﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PlayerCircuitData : MonoBehaviour
{

    public Camera PlayerCam;
    public GameObject Sensor;

    public int currentWaypointIndex;
    public int currentPlace;
    public int currentLap = 1;

    public TextMeshProUGUI lapText;
    public TextMeshProUGUI placeText;

    public List<float> waypointDistances = new List<float>();

    public float totalDistance;

    public bool isFinished;


    






    public void UpdatePlacementText()
    {

        switch (currentPlace)
        {


            case 0:
                placeText.text = "1st";
                break;
            case 1:
                placeText.text = "2nd";
                break;
            case 2:
                placeText.text = "3rd";
                break;

            default:
                break;
                



        }
       
    }

    public void updateCurrentWaypoint()
    {
        if (!isFinished)
        {
            if (currentWaypointIndex < CircuitUIManager.Instance.Waypoints.Length - 1)
            {
                currentWaypointIndex++;

            }

            CircuitUIManager.Instance.CalculateWaypointDistance();
        }
    }

    public void UpdateLap()
    {
        if (currentWaypointIndex == CircuitUIManager.Instance.Waypoints.Length - 1)
        {
            currentLap++;
            currentWaypointIndex = 0;
            lapText.text = "Lap:" + currentLap.ToString() + "/" + CircuitUIManager.Instance.numberofLaps.ToString();
         
        }

        if(currentLap > CircuitUIManager.Instance.numberofLaps)
        {

            lapText.gameObject.SetActive(false);
            isFinished = true;
            placeText.gameObject.SetActive(false);
            Sensor.GetComponent<BoxCollider>().enabled = false;
            CircuitUIManager.Instance.OrderedList.Add(gameObject);
            CircuitUIManager.Instance.OnGameEnded();
        }

    }

}
