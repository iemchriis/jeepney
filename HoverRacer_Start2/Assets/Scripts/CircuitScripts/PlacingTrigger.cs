﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacingTrigger : MonoBehaviour
{
  

    void OnTriggerExit(Collider other)
    {
        //If the player has passed through the LapChecker (isRead) OR if Debug Mode is enabled (debugMode)
        //AND the object passing through this trigger is tagged as "PlayerSensor"...
        if (other.gameObject.tag == "PlayerSensor")
        {


            CircuitUIManager.Instance.CalculateWaypointDistance();

        }

        if (other.gameObject.tag == "Waypoints" && gameObject.tag == "PlayerSensor")
        {


            transform.root.GetComponent<PlayerCircuitData>().updateCurrentWaypoint();
    


        }
    }


  

}

