﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCircuitManager : MonoBehaviour
{

    public PlayerCircuitData[] Players;


    // Start is called before the first frame update
    void Start()
    {
        switch(PlayerDataManager.Instance.NumberOfPlayers)
        {

            case 2:
                SetUpPlayers(2);
                break;
            case 3:
                SetUpPlayers(3);
                break;
            case 4:
                SetUpPlayers(4);
                break;
            default:
                break;
        }
    }


    private void Update()
    {

        for(int i = 0; i<Players.Length;i++)
        {


        }

    }



    void SetUpPlayers(int num)
    {

        for(int i = 0; i < num; i++)
        {
            Players[i].gameObject.SetActive(true);

        }


        switch(num)
        {

            case 1:
                Players[0].PlayerCam.rect = new Rect(0f, 0f, 1f, 1f);
                break;
            case 2:
                Players[0].PlayerCam.rect = new Rect(0f, 0f, 0.5f, 1);
                Players[1].PlayerCam.rect = new Rect(0.5f, 0f, 0.5f, 1);
                break;

            case 3:
                Players[0].PlayerCam.rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
                Players[1].PlayerCam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                Players[2].PlayerCam.rect = new Rect(0f, 0f, 0.5f, 0.5f);
                break;

            case 4:
                Players[0].PlayerCam.rect = new Rect(0f, 0.5f, 0.5f, 0.5f);
                Players[1].PlayerCam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                Players[2].PlayerCam.rect = new Rect(0f, 0f, 0.5f, 0.5f);
                Players[3].PlayerCam.rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
                break;

            default:

                break;


        }


    }



}
