﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
public class WayPointManager : MonoBehaviour
{

  // public PlayerCircuitData[] Points;

    public List<Transform> WayPoints = new List<Transform>();
     
    public Transform currentWaypoint;

    public int waypointIndex;

    public float[] distances;

    public float totalDistance;
    void Start()
    {
        if (currentWaypoint == null)
        {

            currentWaypoint = WayPoints[0];
        }

        calcObjectToWayPoints();

    }

    void Update()
    {
        //PointsSample = PointsSample.OrderBy(point => Vector3.Distance(currentWaypoint.transform.position, point.transform.position)).ToArray();

       //   Points = Points.OrderByDescending(x => x.GetComponent<PlayerCircuitData>().).ToArray();
        // UpdateWaypointIndex();

    }


    public void UpdateWaypointIndex()
    {

        // for(int i = 0; i < PlayerDataManager.Instance.NumberOfPlayers; i ++)
        //{
        // Points[i].UpdatePosition();
        //}
    }


    void calcObjectToWayPoints()
    {

        for (var i = 0; i < WayPoints.Count; i++)
        {
           

                distances[i] = Vector3.Distance(WayPoints[i].position , currentWaypoint.transform.position);
           
                totalDistance += distances[i];


           
        }
    }
}
