﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
public class CircuitUIManager : MonoBehaviour
{

    public static CircuitUIManager Instance { get; set; }

    public PlayerCircuitData[] Players;


    public Transform[] Waypoints;

    public int numberofLaps;

    public List<GameObject> OrderedList = new List<GameObject>();

    public GameObject RankingsPanel;
    public Text[] RankingsText;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        CalculateWaypointDistance();

    }


    public void OnGameEnded()
    {

        if (Players.Length == OrderedList.Count)
        {

            for (int i = 0; i < OrderedList.Count; i++)
            {
                RankingsText[i].gameObject.SetActive(true);
                RankingsText[i].text = i + 1.ToString() + ":" + OrderedList[i].name;
            }
            RankingsPanel.SetActive(true);
        }
        FormatRankingsText();
    }


    public void CalculateWaypointDistance()
    {



        for (int i = 0; i < Players.Length; i++)
        {

            Players[i].totalDistance = 0;
            int playerLoop = Players[i].GetComponent<PlayerCircuitData>().currentWaypointIndex;

            //  float distance = Vector3.Distance(Waypoints[b].position, Players[i].transform.GetChild(0).position);
            float distance = (Waypoints[Players[i].currentWaypointIndex].position - Players[i].transform.GetChild(0).position).magnitude;
            Players[i].totalDistance = distance;

        }

        Players = Players.OrderBy(x => x.GetComponent<PlayerCircuitData>().totalDistance).OrderByDescending(x => x.GetComponent<PlayerCircuitData>().currentWaypointIndex).OrderByDescending(x => x.GetComponent<PlayerCircuitData>().currentLap).ToArray();

        //Players = Players.OrderByDescending(x => x.GetComponent<PlayerCircuitData>().currentLap).ToArray();
        UpdatePlacementText();

    }

    void UpdatePlacementText()
    {

        for (int i = 0; i < Players.Length; i++)
        {

            Players[i].currentPlace = i;


            switch (Players[i].currentPlace)
            {
                case 0:
                    Players[i].placeText.text = "1st";
                  
                    break;
                case 1:
                    Players[i].placeText.text = "2nd";
                  
                    break;
                case 2:
                    Players[i].placeText.text = "3rd";

                    break;

                default:
                    int placement = i += 1;
                    Players[i].placeText.text = placement.ToString() + "th";
                 
                    break;

            }

        }
    }


    private void FormatRankingsText()
    {
        for (int i = 0; i < OrderedList.Count; i++)
        {

            Players[i].currentPlace = i;


            switch (Players[i].currentPlace)
            {
                case 0:
                 
                    RankingsText[i].text = "1st:" + OrderedList[i].name;
                    break;
                case 1:
    
                    RankingsText[i].text = "2nd:" + OrderedList[i].name;
                    break;
                case 2:
               
                    RankingsText[i].text = "3rd:" + OrderedList[i].name;
                    break;

                default:
                    int placement = i += 1;
                  
                    RankingsText[i].text = placement.ToString() + "th:" + OrderedList[i].name;
                    break;

            }

        }
    }

}
